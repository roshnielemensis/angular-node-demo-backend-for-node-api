# Angular node demo backend for node api

## Build RESTFul API With Node and Express

Create RESTful API with Node and Express.js, not just that to handle the data we will learn to use mongoDB.

You need to build the backend separately, so execute the below command to invoke the REST API development with Node and Express.js.

```terminal
mkdir node-rest-api && cd node-rest-api
```

### Create **node-backend** Folder

Evoke npm initializer to set up a new npm package in the **node-backend** folder.

```terminal
npm init
```

Define name, version, description, main and author name to new node project
it will generate **package.json** file

Run command to install imperative npm packages which will help us to create REST APIs for our Angular 13 CRUD system.

```terminal
npm install express cors body-parser mongoose
```

### To automate the server restarting process install nodemon package as a dev dependency

```terminal
npm install nodemon --save-dev
```

### Create the Book model or schema, create node-backend/model folder

Create a **Book.js** file within and place the below code

```javascript
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let Book = new Schema({
  name: {
    type: String
  },
  price: {
    type: String
  },
  description: {
    type: String
  }
}, {
  collection: 'books'
})
module.exports = mongoose.model('Book', Book)
```

### The REST API routes using Express js in a node project. Create node-backend/routes folder

Create **book.routes.js** file

Add code in **book.routes.js** file.

Now, you need to sum up all the code and conjugate at one place so that we can run our backend and propel the CRUD app development forward.

### Create and add the below code in index.js file

```javascript
const express = require('express')
const path = require('path')
const mongoose = require('mongoose')
const cors = require('cors')
const bodyParser = require('body-parser')
mongoose
  .connect('mongodb://127.0.0.1:27017/mydatabase')
  .then((x) => {
    console.log(`Connected to Mongo! Database name: "${x.connections[0].name}"`)
  })
  .catch((err) => {
    console.error('Error connecting to mongo', err.reason)
  })
const bookRoute = require('./routes/book.routes')
const app = express()
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: false,
  }),
)
app.use(cors())
// Static directory path
app.use(express.static(path.join(__dirname, 'dist/angular-mean-crud-tutorial')))
// API root
app.use('/api', bookRoute)
// PORT
const port = process.env.PORT || 8000
app.listen(port, () => {
  console.log('Listening on port ' + port)
})
// 404 Handler
app.use((req, res, next) => {
  next(createError(404))
})
// Base Route
app.get('/', (req, res) => {
  res.send('invaild endpoint')
})
app.get('*', (req, res) => {
  res.sendFile(
    path.join(__dirname, 'dist/angular-mean-crud-tutorial/index.html'),
  )
})
// error handler
app.use(function (err, req, res, next) {
  console.error(err.message)
  if (!err.statusCode) err.statusCode = 500
  res.status(err.statusCode).send(err.message)
})
```

To start the node and express server, you must have MongoDB installed on your local development system.

Once the MongoDB community edition has been set up, then run the below command to evoke the database

```terminal
brew services start mongodb-community
```

Next, execute the command while staying in the server folder **(node-backend)**

```terminal
nodemon
```

Here is your bash URL for REST API built with Node and Express **http://localhost:8000/api**

The endpoints we created and you can use these to handle the CRUD operations with Angular application:

**Methods**    **Endpoints**
GET         /api,
POST        /add-book,
GET         /read-book/id,
PUT         /update-book/id,
DELETE      /delete-book/id,
