const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let Book = new Schema({
  bookno:{
    type: Number
  },
  bookname: {
    type: String
  },
  price: {
    type: String
  },
  authorname: {
    type: String
  }
}, {
  collection: 'books'
})
module.exports = mongoose.model('Book', Book)